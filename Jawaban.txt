************************** Soal 1 Membuat Database **************************

C:\xampp\mysql>cd \xampp\mysql\bin\
C:\xampp\mysql\bin>mysql -uroot
Welcome to the MariaDB monitor.  Commands end with ; or \g.
Your MariaDB connection id is 14
Server version: 10.4.21-MariaDB mariadb.org binary distribution

Copyright (c) 2000, 2018, Oracle, MariaDB Corporation Ab and others.

Type 'help;' or '\h' for help. Type '\c' to clear the current input statement.
MariaDB [(none)]> show databases;

MariaDB [(none)]> create database myshop;
Query OK, 1 row affected (0.002 sec)

MariaDB [(none)]> show databases;
+--------------------+
| Database           |
+--------------------+
| data               |
| information_schema |
| myshop             |
| mysql              |
| performance_schema |
| phpmyadmin         |
| test               |
+--------------------+
7 rows in set (0.006 sec)


************************** Soal 2 Membuat Table di Dalam Database **************************
MariaDB [(none)]> use myshop;
Database changed

--------- TABLE USER ---------
MariaDB [myshop]> create table users (
    -> id int primary key auto_increment,
    -> name varchar(255),
    -> email varchar(255),
    -> password varchar(255)
    -> );
Query OK, 0 rows affected (0.040 sec)

MariaDB [myshop]> show tables;
+------------------+
| Tables_in_myshop |
+------------------+
| users            |
+------------------+
1 row in set (0.001 sec)

MariaDB [myshop]> describe users;
+----------+--------------+------+-----+---------+----------------+
| Field    | Type         | Null | Key | Default | Extra          |
+----------+--------------+------+-----+---------+----------------+
| id       | int(11)      | NO   | PRI | NULL    | auto_increment |
| name     | varchar(255) | YES  |     | NULL    |                |
| email    | varchar(255) | YES  |     | NULL    |                |
| password | varchar(255) | YES  |     | NULL    |                |
+----------+--------------+------+-----+---------+----------------+
4 rows in set (0.018 sec)

--------- TABLE CATEGORIES ---------
MariaDB [myshop]> create table categories (
    -> id int primary key auto_increment,
    -> name varchar(255)
    -> );
Query OK, 0 rows affected (0.034 sec)

MariaDB [myshop]> show tables;
+------------------+
| Tables_in_myshop |
+------------------+
| categories       |
| users            |
+------------------+
2 rows in set (0.001 sec)

MariaDB [myshop]> describe categories;
+-------+--------------+------+-----+---------+----------------+
| Field | Type         | Null | Key | Default | Extra          |
+-------+--------------+------+-----+---------+----------------+
| id    | int(11)      | NO   | PRI | NULL    | auto_increment |
| name  | varchar(255) | YES  |     | NULL    |                |
+-------+--------------+------+-----+---------+----------------+
2 rows in set (0.021 sec)

--------- TABLE ITEMS ---------
MariaDB [myshop]> create table items (
    -> id int primary key auto_increment,
    -> name varchar(255),
    -> description varchar(255),
    -> price int,
    -> stock int,
    -> category_id int,
    -> foreign key (category_id) references categories(id));
Query OK, 0 rows affected (0.037 sec)

MariaDB [myshop]> show tables;
+------------------+
| Tables_in_myshop |
+------------------+
| categories       |
| items            |
| users            |
+------------------+
3 rows in set (0.001 sec)

MariaDB [myshop]> describe items;
+-------------+--------------+------+-----+---------+----------------+
| Field       | Type         | Null | Key | Default | Extra          |
+-------------+--------------+------+-----+---------+----------------+
| id          | int(11)      | NO   | PRI | NULL    | auto_increment |
| name        | varchar(255) | YES  |     | NULL    |                |
| description | varchar(255) | YES  |     | NULL    |                |
| price       | int(11)      | YES  |     | NULL    |                |
| stock       | int(11)      | YES  |     | NULL    |                |
| category_id | int(11)      | YES  | MUL | NULL    |                |
+-------------+--------------+------+-----+---------+----------------+
6 rows in set (0.018 sec)


************************** Soal 3 Memasukkan Data pada Table **************************

 --------- TABLE USER ---------
MariaDB [myshop]> insert into users (name, email, password) values ("John Doe","john@doe.com","john123"), ("Jane Doe","jane@doe.com","jenita123");
Query OK, 2 row affected (0.10 sec)
Records: 2  Duplicates: 0  Warnings: 0

MariaDB [myshop]> select * from users;
+----+----------+--------------------------+-----------+
| id | name     | email                    | password  |
+----+----------+--------------------------+-----------+
|  1 | John Doe | john@doe.com             | john123   |
|  2 | Jane Doe | jane@doe.com             | jenita123 |
+----+----------+--------------------------+-----------+
2 rows in set (0.000 sec)

--------- TABLE CATEGORIES ---------
MariaDB [myshop]> insert into categories (name) values("gadget"),("cloth"),("men"),("women"),("branded");
Query OK, 5 row affected (0.06 sec)
Records: 5  Duplicates: 0  Warnings: 0

MariaDB [myshop]> select * from categories;
+----+---------+
| id | name    |
+----+---------+
|  1 | gadget  |
|  2 | cloth   |
|  3 | men     |
|  4 | women   |
|  5 | branded |
+----+---------+
5 rows in set (0.000 sec)

--------- TABLE ITEMS ---------
MariaDB [myshop]> insert into items insert into items(name, description, price, stock, category_id) values ("sumsang b50","hape keren dari merk sumsang", 4000000, 100, 1),
("uniklooh","baju keren dari brand ternama", 500000, 50, 2),("imho watch","jam tangan anak yang jujur banget", 2000000, 10, 1);
Query OK, 3 rows affected (0.08 sec)
Records: 3  Duplicates: 0  Warnings: 0

MariaDB [myshop]> select * from items;
+----+-------------+-----------------------------------+---------+-------+-------------+
| id | name        | description                       | price   | stock | category_id |
+----+-------------+-----------------------------------+---------+-------+-------------+
|  1 | sumsang b50 | hape keren dari merek sumsang     | 4000000 |   100 |           1 |
|  2 | uniklooh    | baju keren dari brand ternama     |  500000 |    50 |           2 |
|  3 | imho watch  | jam tangan anak yang jujur banget | 2000000 |    10 |           1 |
+----+-------------+-----------------------------------+---------+-------+-------------+
3 rows in set (0.000 sec)


************************** Soal 4 Mengambil Data dari Database4 **************************

MariaDB [myshop]> select id, name, email from users;
+----+----------+--------------------------+
| id | name     | email                    |
+----+----------+--------------------------+
|  2 | John Doe | john@doe.com             |
|  3 | Jane Doe | jane@doe.com             |
+----+----------+--------------------------+
3 rows in set (0.000 sec)

MariaDB [myshop]> select * from items where price > 1000000;
+----+-------------+-----------------------------------+---------+-------+-------------+
| id | name        | description                       | price   | stock | category_id |
+----+-------------+-----------------------------------+---------+-------+-------------+
|  1 | sumsang b50 | hape keren dari merek sumsang     | 4000000 |   100 |           1 |
|  3 | imho watch  | jam tangan anak yang jujur banget | 2000000 |    10 |           1 |
+----+-------------+-----------------------------------+---------+-------+-------------+
2 rows in set (0.001 sec)

MariaDB [myshop]> select * from items where name like "uniklo%";
+----+----------+-------------------------------+--------+-------+-------------+
| id | name     | description                   | price  | stock | category_id |
+----+----------+-------------------------------+--------+-------+-------------+
|  2 | uniklooh | baju keren dari brand ternama | 500000 |    50 |           2 |
+----+----------+-------------------------------+--------+-------+-------------+
1 row in set (0.000 sec)


MariaDB [myshop]> select items.name, items.description, items.price, items.stock, items.category_id, categories.name from items inner join categories on items.category_id=categories.id;
ERROR 2006 (HY000): MySQL server has gone away
No connection. Trying to reconnect...
Connection id:    447
Current database: myshop

+-------------+-----------------------------------+---------+-------+-------------+--------+
| name        | description                       | price   | stock | category_id | name   |
+-------------+-----------------------------------+---------+-------+-------------+--------+
| sumsang b50 | hape keren dari merek sumsang     | 4000000 |   100 |           1 | gadget |
| uniklooh    | baju keren dari brand ternama     |  500000 |    50 |           2 | cloth  |
| imho watch  | jam tangan anak yang jujur banget | 2000000 |    10 |           1 | gadget |
+-------------+-----------------------------------+---------+-------+-------------+--------+
3 rows in set (0.010 sec)


************************** Soal 5 Mengubah Data dari Database **************************

MariaDB [myshop]> update items set price = 2500000 where id = 1;
Query OK, 1 row affected (0.006 sec)
Rows matched: 1  Changed: 1  Warnings: 0

MariaDB [myshop]> select * from items;
+----+-------------+-----------------------------------+---------+-------+-------------+
| id | name        | description                       | price   | stock | category_id |
+----+-------------+-----------------------------------+---------+-------+-------------+
|  1 | sumsang b50 | hape keren dari merek sumsang     | 2500000 |   100 |           1 |
|  2 | uniklooh    | baju keren dari brand ternama     |  500000 |    50 |           2 |
|  3 | imho watch  | jam tangan anak yang jujur banget | 2000000 |    10 |           1 |
+----+-------------+-----------------------------------+---------+-------+-------------+
3 rows in set (0.000 sec)
